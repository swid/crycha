use std::num::Float;

pub fn increase_key(input: &mut Vec<u8>) -> bool {
    for byte in input.iter_mut().rev() {
        *byte += 1;
        if *byte > 0 {
            return true;
        }
    }
    return false;
}

trait Inc {
    fn inc(mut self) -> Self;
}

impl Inc for Vec<u8> {
    fn inc(mut self) -> Vec<u8>{
        increase_key(&mut self);
        self
    }
}

pub fn hamming_str(left: &str, right: &str) -> uint {
    let l = left.as_bytes();
    let r = right.as_bytes();
    hamming(l , r)
}

pub fn hamming(l : &[u8], r :&[u8]) -> uint {
    let mut ret: uint = 0;
    for (l1, r1) in l.iter().zip(r.iter()) {
        let mut diff = *l1 ^ *r1;
        for _x in range(0u, 8) {
            if diff & 1 != 0 {
                ret += 1;
            }
            diff = diff >> 1;
        }
    }
    ret
}

fn dec2base64(i: u8) -> Result<char, &'static str> {
    match i {
        0...25 => Ok((i + 65) as char),
        26...51 => Ok((i - 26 + 97) as char),
        52...61 => Ok((i - 4) as char),
        62 => Ok('+'),
        63 => Ok('/'),
        _ => Err("Not a Base64 encodable value")
    }
}

fn hexchar2dec(c: char) -> Result<u8, &'static str> {
    hexascii2dec(c as u8)
}

fn hexascii2dec(c: u8) -> Result<u8, &'static str> {
    match c {
        b'0'...b'9' => Ok(c - 48),
        b'a'...b'f' => Ok(c - 87),
        _ => Err("Not a hex number")
    }
}

fn byte2hex(c: u8) -> (char, char) {
    let n2c = |n:u8| match n {
        0...9 => (n + 48) as char,
        _     => (n + 87) as char
    };
    (n2c((c & 0b11110000) >> 4), n2c(c & 0b00001111))
}

fn bytes2base64(b1: u8, b2: u8, b3: u8) -> String {
    let mut ret = String::new();
    let x: u32 = b3 as u32 | (b2 as u32 << 8) | (b1 as u32 << 16);
    for i in range(0, 4) {
        ret.push(dec2base64((x >> ((3 - i) * 6))  as u8 & 0b00111111).unwrap());
    }
    ret
}

pub fn from_hex(s: &str) -> Result<Vec<u8>, &'static str>{
    if s.as_bytes().len() % 2 != 0 {
        return Err("Uneven nibble count");
    }
    let mut ret = vec![];
    for part in s.as_bytes().chunks(2) {
        match part {
            [n1, n2] => ret.push(
                try!(hexascii2dec(n1)) * 16 +
                try!(hexascii2dec(n2))
            ),
            _ => unreachable!()
        }
    }
    Ok(ret)
}

pub fn to_hex(bytes: &Vec<u8>) -> String {
    let mut s = String::new();
    for b in bytes.iter() {
        let (n1, n2) = byte2hex(*b);
        s.push(n1);
        s.push(n2);
    }
    s
}

pub fn to_base64(bytes: &Vec<u8>) -> Result<String, &'static str> {
    let mut ret = String::new();
    for tr in bytes.chunks(3) {
        match tr {
            [b1, b2, b3] => ret.push_str(bytes2base64(b1, b2, b3).as_slice()),
            [b1, b2] => ret.push_str(bytes2base64(b1, b2, 0).as_slice()),
            [b1] => ret.push_str(bytes2base64(b1, 0, 0).as_slice()),
            _ => return Err("Error")
        }
    }
    Ok(ret)
}

pub fn to_ascii(bytes: &Vec<u8>) -> String {
    let mut ret = String::new();
    for b in bytes.iter() {
        ret.push(*b as char);
    }
    ret
}

pub fn xor(left: &Vec<u8>, right: &Vec<u8>) -> Result<Vec<u8>, &'static str> {
    let mut ret = vec![];
    for (lb, rb) in left.iter().zip(right.iter().cycle()) {
        ret.push(lb.bitxor(rb));
    }
    Ok(ret)
}

fn bytewise_xor_decode(key: u8, input: &Vec<u8>) -> Vec<u8> {
    let mut ret = vec![];
    for b in input.iter() {
        ret.push(*b ^ key);
    }
    //println!("{}", to_ascii(&ret));
    return ret;
}

fn ascii_cleartext_score(ct: &Vec<u8>) -> f32 {
    let freq = [('e',12.02f32),
                ('t',9.10f32),
                ('a',8.12f32),
                ('o',7.68f32),
                ('i',7.31f32),
                ('n',6.95f32),
                ('s',6.28f32),
                ('r',6.02f32),
                ('h',5.92f32),
                ('d',4.32f32),
                ('l',3.98f32),
                ('u',2.88f32),
                ('c',2.71f32),
                ('m',2.61f32),
                ('f',2.30f32),
                ('y',2.11f32),
                ('w',2.09f32),
                ('g',2.03f32),
                ('p',1.82f32),
                ('b',1.49f32),
                ('v',1.11f32),
                ('k',0.69f32),
                ('x',0.17f32),
                ('q',0.11f32),
                ('j',0.10f32),
                ('z',0.07f32)];

    let mut count: [uint, ..255] = [0, ..255];
    let mut score = 1.0;
    // Assuming english letter probabilities and printable characters
    let is_print = |c| match c {
        0           => true,
        0x20...0x73 => true,
        _           => false
    };

    let is_letter = |c| match c {
        0x61...0x7a => true,
        0x41...0x5a => true,
        _           => false
    };

    let to_lower = |c| match c {
        0x41...0x5a => c + 32,
        _           => c
    };

    let is_lower = |c| match c {
        0x61...0x7a => true,
        _           => false
    };

    for b in ct.iter() {
        if !is_print(*b) { score *= 0.9 };
        if is_lower(*b) { score *= 1.05 };
        if is_letter(*b) {
            // Letters make cleartext more probable
            score *= 1.05;
            // Collect for stats
            let l = to_lower(*b) as uint;
            count[l] += 1;
        }
    }
    for i in freq.iter() {
        let (f, p) = *i;
        let m = 1f32 + p/100f32;
        if count[f as uint] > 0 {
            score *= m.powf(count[f as uint] as f32);
        }
    }
    score
}


pub fn bruteforce(input: &Vec<u8>) -> (u8, f32, Vec<u8>) {
    let mut max_score  = 0f32;
    let mut key: u8 = 0;
    let mut ret: Vec<u8> = vec![];
    for x in range(0u, 256) {
        let dec = bytewise_xor_decode(x as u8, input);
        let score = ascii_cleartext_score(&dec);
        if score > max_score {
            max_score  = score;
            key = x as u8;
            ret = dec;
        }
    }
    (key, max_score, ret)
}

pub trait CryptoData {
    fn from_hex(&str) -> Result<Self, &'static str>;
    fn to_hex(&self) -> String;

    //fn from_base64(input: &str) -> Self;
    fn to_base64(&self) -> String;

    fn to_ascii(&self) -> String;

    fn xor(&self, &Self) -> Self;
}

impl CryptoData for Vec<u8> {
    fn from_hex(input: &str) -> Result<Vec<u8>, &'static str> {
        from_hex(input)
    }

    fn to_hex(&self) -> String {
        to_hex(self)
    }

    fn to_base64(&self) -> String {
        to_base64(self).unwrap()
    }

    fn to_ascii(&self) -> String {
        to_ascii(self)
    }

    fn xor(&self, with: &Vec<u8>) -> Vec<u8> {
        xor(self, with).unwrap()
    }
}

#[test]
fn test_trait_from_hex() {
    let x = CryptoData::from_hex("deadbeef");
    assert_eq!(x.unwrap(), vec![222, 173, 190, 239]);
}

#[test]
fn test_trait_to_hex() {
    let y = vec![171, 205, 239, 1].to_hex();
    assert_eq!(y.as_slice(), "abcdef01");
}

#[test]
fn test_trait_to_base64() {
    let b = vec![0 ,0, 0].to_base64();
    assert_eq!(b.as_slice(), "AAAA");
}

#[test]
fn test_trait_xor() {
    let xor = vec![0,1,0,1].xor(&vec![1,1,1,1,1]);
    assert_eq!(xor, vec![1,0,1,0]);
    let xor = vec![0,1,2,3].xor(&vec![255]);
    assert_eq!(xor, vec![255,254,253,252]);
}

#[test]
fn test_byte_to_hex() {
    assert_eq!(byte2hex(255), ('f', 'f'));
    assert_eq!(byte2hex(0), ('0', '0'));
}

#[test]
fn set1_challenge1() {
    let x = from_hex("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d").unwrap();
    assert_eq!(String::from_str("SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"), to_base64(&x).unwrap());
}

#[test]
fn set1_challenge2() {
    let l = from_hex("1c0111001f010100061a024b53535009181c").unwrap();
    let r = from_hex("686974207468652062756c6c277320657965").unwrap();
    assert_eq!("746865206b696420646f6e277420706c6179", to_hex(&(xor(&l, &r).unwrap())).as_slice());
}

#[test]
fn set1_challenge3() {
    let input = from_hex("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736").unwrap();
    let (key, _score, _dec) = bruteforce(&input);
    assert_eq!(88, key);
}

#[test]
fn set1_challenge5() {
    let input="Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal";
    let expect = String::from_str("0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f");
    let key = vec!['I' as u8, 'C' as u8, 'E' as u8];
    let res = xor(&String::from_str(input).into_bytes(), &key);
    assert_eq!(to_hex(&res.unwrap()), expect);
}

#[test]
fn test_hamming() {
    assert_eq!(hamming_str("this is a test", "wokka wokka!!!"), 37);
    assert_eq!(hamming_str("0", "1"), 1);
}

#[test]
fn test_increase_key() {
    let mut test = vec![0, 0];
    increase_key(&mut test);
    assert_eq!(vec![0, 1], test);
    increase_key(&mut test);
    assert_eq!(vec![0, 2], test);
    let mut test2 = vec![13, 1, 255, 255];
    increase_key(&mut test2);
    assert_eq!(vec![13, 2, 0, 0], test2);
    let mut test = vec![254];
    assert_eq!(increase_key(&mut test), true);
    assert_eq!(increase_key(&mut test), false);
}

#[test]
fn test_vec_inc() {
    let x = vec![0u8, 0];
    assert_eq!(vec![0u8, 1], x.inc());
}
