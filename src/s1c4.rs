use std::io::BufferedReader;
use std::io::File;
use std::os;

mod lib;

fn main() {
    let args = os::args();
    let path = Path::new(if args.len() > 1 {
        "data/set1challenge4.txt"
    } else {
        args[1].as_slice()
    });
    let threshold = if args.len() > 2 {match from_str::<f32>(args[2].as_slice()) {
        Some(x) => x,
        _         => 5f32
    }} else { 5f32 };
    let mut file = BufferedReader::new(File::open(&path));
    let mut max_score = 0f32;
    let mut max_key = 0u8;
    let mut max_dec = vec![];
    for line in file.lines() {
        let input = lib::from_hex(line.unwrap().trim().as_slice()).unwrap();
        let (key, score, dec) = lib::bruteforce(&input);
        if score > threshold {
            println!("Key: {}, Score: {}, Text: {}", key, score, lib::to_ascii(&dec));
        }
        if score > max_score {
            max_score = score;
            max_key = key;
            max_dec = dec;
        }
    }
    println!("Winner: Key: {}, Score: {}, Text: {}", max_key, max_score, lib::to_ascii(&max_dec));
}
