use std::io::BufferedReader;
use std::io::File;
use std::os;

mod lib;

fn guess_keylength(input: &Vec<u8>, min: uint, max: uint) -> Vec<(uint, f32)> {
    let mut ret = vec![];
    for len in range(min, max+1) {
        println!("Checking key length: {}", len);
        let mut gen = input.chunks(len).take(2);
        let p1 = gen.next().unwrap();
        let p2 = gen.next().unwrap();

        let h = lib::hamming(p1, p2) as f64;
        println!("{}", h/len as f64);
    }
    ret
}

fn main() {
    let args = os::args();
    let path = Path::new(if args.len() > 1 {
        args[1].as_slice()
    } else {
        "data/set1challenge6.txt"
    });
    let data: Vec<u8> = File::open(&path).read_to_end().unwrap();
    println!("{}", data);
    guess_keylength(&data, 2, 40);
}
